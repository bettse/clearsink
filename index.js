/* global require, console */

//Core includes
var util = require('util');
var fs = require('fs');

var process = require('process');
var clearapp = require('clearapp');
var changeCase = require('change-case');
var todotxt = require('todo.txt');


var user = process.env.USER;
var dropboxList = new todotxt.TodoList();
dropboxList.filename = '/Users/' + user + '/Dropbox/todo/todo.txt';

clearapp.total(function(error, total) {
  console.log(util.format("I've got %s problems, but a todo list ain't one.", total));
});


//Conversion of Clear -> Todo.txt (destructive)

clearapp.tasks(function(error, tasks) {
  var i = 0;
  var taskList = [];

  //TODO: read in dropboxfile first
  //TODO: dedup dropboxfile and clear app

  for(i = 0; i < tasks.length; i++) {
    var aTask = tasks[i]; // { list_title: 'Bar chart list', title: 'Good names' },

    var project = changeCase.pascalCase(aTask.list_title);
    var taskString = util.format("%s +%s", aTask.title, project);
    var newItem = new todotxt.TodoItem(taskString);

    dropboxList.add(newItem);
    taskList.push(newItem.toString());
  }

  fs.writeFileSync(dropboxList.source(), taskList.join('\n'));

});


//Conversion of Todo.txt -> Clear-ish?
function dropboxToClear() {
  var dropboxList = new todotxt.TodoList();
  dropboxList.filename = '/Users/' + user + '/Dropbox/todo/todo.txt';

  fs.readFile(dropboxList.source(), 'utf8', function(error, data) {
    var i = 0;
    dropboxList.parse(data);
    var todoList = dropboxList.listAll();
    var taskList = [];
    for(i = 0; i < todoList.length; i++) {
      var aItem = todoList[i];
      var projects = aItem.projects;
      for (var j = 0; j < projects.length; j++) {
        var aProject = projects[j];
        var listTitle = changeCase.titleCase(aProject);
        var taskTitle = aItem.text.replace('+' + aProject, '').trim();
        var task = {'list_title': listTitle, 'title': taskTitle};
        taskList.push(task);
        //TODO: Needs a clearapp method to create a new task and list (if needed)
      }
    }


    console.log(taskList);
  });

}

